package com.gl.Graded.Project6;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.gl.Graded.Project6.model.Book;
import com.gl.Graded.Project6.repositories.BookRepositories;
import com.gl.Graded.Project6.service.BookService;

@SpringBootTest
class ApplicationTests {

	@Test
	void contextLoads() {
	}
	
	@Mock
	private BookRepositories bookRepository;
	
	private BookService bookService;
	
	@Test
	public void getAllBookServiceTest() {
		Book book1 = new Book("James J. Gosling","Java","analysis of the language","BioGraphy",5,1000.0,"ENADU","JAVA",1);
		Book book2 = new Book("Dennis_Ritchi","c","c_language","Biography",15,500.0,"SAKSHI","First programming language",2);
		List<Book> books = Arrays.asList(book1,book2);
		when(bookRepository.findAll()).thenReturn(books);
		List<Book> testResult = bookService.getAllBooks();
		assertEquals(testResult.size(), books.size());
	}
	

	
}
