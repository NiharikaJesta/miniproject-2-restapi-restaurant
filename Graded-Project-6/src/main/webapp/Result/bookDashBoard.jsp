<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<div align="center">
		<h1>****** Book List ******</h1>
		<h3>
			<a href="bookForm">New Book</a>
		</h3>
		

		<table border="1">

			<th>bookId</th>
			<th>bookName</th>
			<th>authorname</th>
			<th>description</th>
			<th>genre</th>
			<th>noOfCopiesSold</th>
			<th>titlename</th>
			<th>publicationname</th>
			<th>price</th>
			<th>Action</th>

			<c:forEach var="contact" items="${bookList}" varStatus="status">
				<tr>

					<td>${contact.bookId}</td>
					<td>${contact.bookName}</td>
					<td>${contact.authorname}</td>
					<td>${contact.description}</td>
					<td>${contact.genre}</td>
					<td>${contact.noOfCopiesSold}</td>
					<td>${contact.titlename}</td>
					<td>${contact.publicationname}</td>
					<td>${contact. price}</td>
					<td><a href="/updateBook/${contact.bookId}">Edit</a>
						&nbsp;&nbsp;&nbsp;&nbsp; <a href="/deleteBook/${contact.bookId}">Delete</a></td>
				</tr>

			</c:forEach>
		</table>
		<br>
		<h3>
			<a href="backTotoAdminDashBoard">Back To Admin DashBoard</a>
		</h3>
	</div>

</body>
</html>