<%@taglib uri = "http://www.springframework.org/tags/form"
 prefix = "form"%>

<html>
   <head>
      <title>Spring MVC Form Handling</title>
   </head>

   <body>
   <div align="center">
  <h1> <form >***User Form***</form></h1>
      <form:form method = "POST" action = "/updated" modelAttribute="ubookData">
         <table>
         	<tr>
               <td><form:label path = "bookId">BookId</form:label></td>
               <td><form:input path = "bookId" /></td>
            </tr>
            <tr>
               <td><form:label path = "bookName">BookName</form:label></td>
               <td><form:input path = "bookName" /></td>
            </tr>
            <tr>
               <td><form:label path = "authorname">AuthorName</form:label></td>
               <td><form:input path = "authorname" /></td>
            </tr>
            <tr>
               <td><form:label path = "description">Description</form:label></td>
               <td><form:input path = "description" /></td>
            </tr>
            <tr>
               <td><form:label path = "genre">Genre</form:label></td>
               <td><form:input path = "genre" /></td>
            </tr>
             <tr>
               <td><form:label path = "noOfCopiesSold">NoOfCopiesSold</form:label></td>
               <td><form:input path = "noOfCopiesSold" /></td>
            </tr>
            <tr>
               <td><form:label path = "titlename">TitleName</form:label></td>
               <td><form:input path = "titlename" /></td>
            </tr>
            <tr>
               <td><form:label path = "publicationname">PublicationName</form:label></td>
               <td><form:input path = "publicationname" /></td>
            </tr>
            <tr>
               <td><form:label path = "price">Price</form:label></td>
               <td><form:input path = "price" /></td>
            </tr>
            <tr>
               <td colspan = "2">
                  <input type = "submit" value = "UPDATE_FORM"/>
               </td>
            </tr>
         </table>  
      </form:form>
    </div>
   </body>
   
</html>