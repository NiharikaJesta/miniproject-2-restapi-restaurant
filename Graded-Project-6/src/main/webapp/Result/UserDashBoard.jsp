<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<body>
	<div align="center">
		<h1>****** Book List ******</h1>
		<!--<input type="button" value="SEARCH BY ID/Author"
			onclick="window.location.href='searchBook/{bookId}" />-->

		<br> <a href="/getfavouriteBooks"> My Favourite Books </a>
		<table border="1">

			<th>bookId</th>
			<th>bookName</th>
			<th>authorname</th>
			<th>description</th>
			<th>genre</th>
			<th>noOfCopiesSold</th>
			<th>titlename</th>
			<th>publicationname</th>
			<th>price</th>
			<th>Add-To-Favourite</th>

			<c:forEach var="contact" items="${bookList}" varStatus="status">
				<tr>

					<td>${contact.bookId}</td>
					<td>${contact.bookName}</td>
					<td>${contact.authorname}</td>
					<td>${contact.description}</td>
					<td>${contact.genre}</td>
					<td>${contact.noOfCopiesSold}</td>
					<td>${contact.titlename}</td>
					<td>${contact.publicationname}</td>
					<td>${contact. price}</td>
					<td><a href="/addToFavorite/${contact.bookId}">CLICK</a></td>
				</tr>

			</c:forEach>
		</table>
		${msg}
		<h4>
			<a href="/searchByIdForm">Search-BookById</a> <br> <a
				href="/finfByAuthorNameForm">Search-AuthorName</a> <br> <a
				href="/finfByPublicationNameForm">Search-PublicationName</a> <br>
			<a href="/finfByTitleNameForm">Search-TitleName</a> <br> <a
				href="/sortByPrize">Sort-BooksByPrize</a> <br> <a
				href="/findyPriceRange">Find-PriceRange</a>
		</h4>

		<h3>
			<a href="/logout">LOGOUT</a>
		</h3>
	</div>
</body>
</html>