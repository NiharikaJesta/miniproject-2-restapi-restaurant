<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<div align="center">
		<h1>******Favorite Book List ******</h1>



		<table border="1">


			<th>BookId</th>
			<th>Bookname</th>
			<tr>

			
			
			<c:forEach var="fav" items="${favList}" varStatus="status">
				<tr>

					<td>${fav.book.bookId}</td>
					<td>${fav.book.bookName}</td>
				</tr>

			</c:forEach>
			
			
			
		</table>
		<br>
		<h3>
			<a href="/dashBoard">Back To User DashBoard</a>
		</h3>
	</div>
</body>
</html>