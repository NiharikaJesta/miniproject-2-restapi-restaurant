<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	
	
	
		<div align="center">
		<h1>****** BOOK Details ******</h1>
		
		

		<table border="1">

			<th>bookId</th> 
			<th>bookName</th>
			<th>price</th>
			<c:forEach var="bookData" items="${bookData}" varStatus="status">
				<tr>
					<td>${bookData.bookId}</td>
					<td>${bookData.bookName}</td>
					<td>${bookData.price}</td>
				</tr>
			</c:forEach>
		</table>
		<br>
		<h3>
			<a href="/backTotoUserDashBoard">Back To User DashBoard</a>
		</h3>
		<br>
		<h3>
			<a href="/logout">LOGOUT</a>
		</h3>
	</div>
					
				
</body>
</html>