package com.gl.Graded.Project6.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="User")
public class User {
	@Id   //primary key
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="UserId",nullable = false)
	private int userId;
	@Column(name="FirstName",nullable = false)
	private String firstName;
	@Column(name="LastName",nullable = false)
	private String lastName;
	@Column(name="Email",nullable = false)
	private String email;
	@Column(name="Password",nullable = false)
	private String password;
	
	
	//default constructor
	public User() {
		
	}
	
	
	//generating field constructor
	public User(int userId, String firstName, String lastName, String email, String password) {
		super();
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
	}
	
	
	//generating getter and setters
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	@OneToMany(cascade =CascadeType.ALL)
	@JoinColumn(name="userid", referencedColumnName="userId")
	private List<AddToFavourite> favlist;


	public List<AddToFavourite> getFavlist() {
		return favlist;
	}


	public void setFavlist(List<AddToFavourite> favlist) {
		this.favlist = favlist;
	}
	
	
}




