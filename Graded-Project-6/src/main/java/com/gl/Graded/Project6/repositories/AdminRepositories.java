package com.gl.Graded.Project6.repositories;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.gl.Graded.Project6.model.Admin;

@Repository //that indicates that the present class is a repository
public interface AdminRepositories extends JpaRepository<Admin,Integer>{//provide crud and functionality methods in JpaRepository interface


	//validation query for user login
	@Query("select admin from Admin admin where admin.email=?1 and admin.password=?2")
	public Optional<Admin> adminValidation(String email,String password);
	
	
	
	
}
