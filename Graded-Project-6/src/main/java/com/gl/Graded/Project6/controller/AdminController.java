package com.gl.Graded.Project6.controller;

import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.gl.Graded.Project6.model.Admin;
import com.gl.Graded.Project6.model.Book;
import com.gl.Graded.Project6.repositories.AdminRepositories;
import com.gl.Graded.Project6.repositories.BookRepositories;
import com.gl.Graded.Project6.service.BookService;

@Controller // annotation that is itself annotated with @Controller and @ResponseBody
public class AdminController {
	@Autowired // getting object of AdminRepositories
	AdminRepositories adminRep;

	@Autowired // getting object of bookService
	BookService bookService;

	@Autowired // getting object of BookRepositories
	BookRepositories bookRep;

	// mapping to get adminDashBoard
	@RequestMapping("/adminDashBoard")
	public ModelAndView getadminDashBoard() {
		return new ModelAndView("adminDashBoard");
	}

	// mapping to get admin login page
	@RequestMapping("/adminLogin")
	public ModelAndView getAdminLoginPage(@ModelAttribute("LoginErrorMessage") String errorMessage) {
		ModelAndView modelAndView = new ModelAndView("adminLoginPage", "adminLoginData", new Admin());
		if (errorMessage == null) {
			return modelAndView;
		} else {
			modelAndView.addObject("LoginErrorMessage", errorMessage);
			return modelAndView;
		}
	}

	// mapping to validate admin
	@RequestMapping("/validateAdminLogin")
	public ModelAndView validateAdmin(@ModelAttribute("adminLoginData") Admin a, HttpSession session) {
		Optional<Admin> adminList = adminRep.adminValidation(a.getEmail(), a.getPassword());
		Admin aFind = null;
		if (adminList.isPresent()) {
			System.out.println("Admin  found");
			aFind = adminList.get();
			return new ModelAndView("redirect:/adminDashBoard");
		} else {
			System.out.println("Admin not found");
			return new ModelAndView("adminLoginPage", "LoginErrorMessage", "Invalid username or password");
		}
	}

	// mapping to get Registration_Form
	@GetMapping("/adminRegistration")
	public ModelAndView getRegistration_Form() {

		return new ModelAndView("adminRegistrationPage", "aregisterData", new Admin());
	}

	// mapping to save Register data
	@PostMapping("/adminRegister")
	public ModelAndView saveRegistrtionData(@ModelAttribute("aregisterData") Admin a) {
		adminRep.save(a);
		return new ModelAndView("redirect:/adminLogin");
	}

	// mapping to logout
	@RequestMapping("/adminLogout")
	public String adminLogout(HttpSession session) {
		session.invalidate();
		return "redirect:/adminLogin";
	}

	// mapping to get back to AdminDashBoard
	@RequestMapping("/backTotoAdminDashBoard")
	public String backTotoAdminDashBoard() {
		return "redirect:/adminDashBoard";
	}
	
    //mapping to searchByIdForm
	@RequestMapping("/asearchByIdForm")
	public ModelAndView searchByIdForm() {
		return new ModelAndView("FindBookByIdForm2", "book", new Book());
	}

	// Admin can search new book (by id, by author
	@RequestMapping("/asearchById")
	public ModelAndView searchByIdOrAuthor(@ModelAttribute("book") Book bk) {
		Book b = bookService.getBookById(bk.getBookId());
		return new ModelAndView("sucess2", "bookData", b);
	}

	// mapping to finfByAuthorNameForm
	@RequestMapping("/afinfByAuthorNameForm")
	public ModelAndView finfByAuthorForm() {
		return new ModelAndView("FindBookByAuthorName2", "book", new Book());

	}

	// User can search book by Author name
	@RequestMapping("/afinfByAuthorName")
	public ModelAndView finfByAuthorName(@ModelAttribute("book") Book bk) {
		Book b = bookRep.findByAuthorname(bk.getAuthorname());
		return new ModelAndView("sucess2", "bookData", b);
	}

	
	
	

	// insert into admin values(1,"admin@gmail.com","admin","1","admin");

}
