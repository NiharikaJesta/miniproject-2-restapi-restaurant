package com.gl.Graded.Project6.security.model;



public class JwtResponse {
	public JwtResponse()
	{
		
	}
	private String token;

	public String getToken() {
		return token;
	}

	public JwtResponse(String token) {
		super();
		this.token = token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
