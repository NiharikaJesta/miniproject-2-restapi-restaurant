package com.gl.Graded.Project6.repositories;
import com.gl.Graded.Project6.model.*;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository//that indicates that the present class is a repository
public interface BookRepositories extends JpaRepository<Book,Integer>{//provide crud and functionality methods in JpaRepository interface

	// search book by Author name
	public Book findByAuthorname(String name);
	
	//search book by title name
	public Book findByTitlename(String name);
	
	// search book by publication name
	public List<Book> findByPublicationname(String name);
	
	//search book by ID name
	public Book findByBookName(String name);
	
	//search book by price range
	@Query("select b from Book b where b.price>?1 and b.price<?2")
	public List<Book> pricerange(double prize1,double prize2);
	
	//sort books by price
	public List<Book> findByOrderByPriceAsc();
	
	//find By BookId And AuthorName
	@Query("select b from Book b where b.bookId=?1 or b.authorname=?2")
	public List<Book> findByBookIdAndAuthorname(int bookid,String authorname);
}
