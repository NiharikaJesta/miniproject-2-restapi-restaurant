package com.gl.Graded.Project6.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.gl.Graded.Project6.model.AddToFavourite;
import com.gl.Graded.Project6.model.Book;
import com.gl.Graded.Project6.model.User;

@Repository//that indicates that the present class is a repository
public interface AddToFavouriteRepositories extends JpaRepository<AddToFavourite, Integer>{//provide crud and functionality methods in JpaRepository interface


	//query to get data of Favorite books
	@Query("select a from AddToFavourite a where a.user=?1")
	public List<AddToFavourite> getBooks(User user);



}
