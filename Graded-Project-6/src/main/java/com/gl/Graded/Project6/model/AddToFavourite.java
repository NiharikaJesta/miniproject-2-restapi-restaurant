
package com.gl.Graded.Project6.model;

import javax.persistence.*;

@Entity
@Table
public class AddToFavourite {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private int favId;

	public int getFavId() {
		return favId;
	}

	public void setFavId(int favId) {
		this.favId = favId;
	}
	
	
	
	
	
	
	@ManyToOne
	@JoinColumn(name="userid", referencedColumnName="userId")
	private User user;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	
	
	
	
	
	@ManyToOne
	@JoinColumn(name="bookid", referencedColumnName = "bookId")
	private Book book;

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public AddToFavourite() {
		
	}
	public AddToFavourite(int favId, User user, Book book) {
		super();
		this.favId = favId;
		this.user = user;
		this.book = book;
	}
	
}

