package com.gl.Graded.Project6.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.gl.Graded.Project6.model.User;

@Repository // this annotation provide that indicates that the present class is a repository
public interface UserRepositories extends JpaRepository<User, Integer> {// provide crud and functionality methods in
																		// JpaRepository interface

	// hibernate query to validate user login
	@Query("select u from User u where u.email=?1 and u.password=?2 ")
	public Optional<User> userValidation(String email, String password);

}
