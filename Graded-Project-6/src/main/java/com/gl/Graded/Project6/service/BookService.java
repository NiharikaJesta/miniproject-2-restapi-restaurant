package com.gl.Graded.Project6.service;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gl.Graded.Project6.model.Book;
import com.gl.Graded.Project6.repositories.BookRepositories;

@Service//service where we write all business logic
public class BookService {

	//getting object of BookRepositories
	@Autowired
	BookRepositories bookRep;
	
	// method to get book
	public Book getBookById(int id) {
		return bookRep.getById(id);
	}
	
	// method to save book
	public Book saveBook(Book book) {
		return bookRep.save(book);
	}
	
	// get all Books
	public List<Book> getAllBooks(){
		List<Book> bookList=new ArrayList<Book>();
		bookRep.findAll().forEach(book->bookList.add(book));
		return bookList;
	}
	
	//delete a book by id
	public void deleteBook(int id) {
		bookRep.deleteById(id);
	}

	//update a book by id
	public void updateBook(Book book) {
		List<Book> bookList=getAllBooks();
		for(Book b:bookList) {
			if(b.getBookId()==book.getBookId()) {
				bookList.remove(book);
				break;
			}
			bookList.add(book);
			bookRep.save(book);
		}
		
	}
	
	//Search book by id And AuthorName
	public List<Book> search(int id,String author){
		List<Book> b=bookRep.findByBookIdAndAuthorname(id,author);
		return b;
	}
}
