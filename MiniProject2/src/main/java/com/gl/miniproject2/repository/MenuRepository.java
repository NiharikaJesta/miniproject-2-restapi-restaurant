package com.gl.miniproject2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gl.miniproject2.entity.Menu;

@Repository
public interface MenuRepository extends JpaRepository<Menu,Long>{

	public Menu findByItemName(String itemname);
}
