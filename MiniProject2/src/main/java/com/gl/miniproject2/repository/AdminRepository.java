package com.gl.miniproject2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gl.miniproject2.entity.Admin;
@Repository
public interface AdminRepository extends JpaRepository<Admin,Long>{
	
	public Admin findByAdminuserNameAndAdminpassword(String username,String password);

}
