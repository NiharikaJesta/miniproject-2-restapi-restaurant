package com.gl.miniproject2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gl.miniproject2.entity.Menu;
import com.gl.miniproject2.entity.MyOrderHistory;
import com.gl.miniproject2.entity.User;
import com.gl.miniproject2.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	UserService userService;

	// User Registration
	@PostMapping("/register")
	public String registerUser(@RequestBody User newuser) {
		userService.saveuser(newuser);
		return "User Registered Successfully";
	}

	// Login User
	@PostMapping("/login")
	public String userLogin(@RequestBody User user) {
		return userService.LoginUser(user);
	}

	// Logout user
	@PostMapping("/logout")
	public String userLogOut(@RequestBody User user) {
		return userService.LogoutUser(user);
	}

	// Displays All Menu Items
	@GetMapping("/showmenu")
	public List<Menu> Showall() {
		return userService.Showall();
	}
	
	// Adding that itemdetails to store
	@PostMapping("/addtostore/{itemname}")
	public List<Menu> addToStore(@PathVariable(value = "itemname") String itemname) {
		return userService.addToStore(itemname);
	}

	// For Ordering the item
	@PostMapping("/order")
	public String SearchItem(@RequestBody MyOrderHistory order_history) {
		return userService.SearchItem(order_history);
	}

	// Displays all orders by username
	@GetMapping("/orderhistory/{username}")
	public List<MyOrderHistory> UserOrders(@PathVariable(value = "username") String username) {
		return userService.UserOrders(username);
	}
	// Final Total Bill for  user
		@GetMapping("/finalbill/{username}")
		public String FinalBill(@PathVariable(value = "username") String username) {
			return userService.FinalBill(username);
		}
	

}
